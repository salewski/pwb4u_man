.TH A.OUT 4 "3B-20 only"
.SH NAME
\*pa.out \- assembler and link editor output
.SH DESCRIPTION
The file name
.B \*pa.out
is the output file from
the assembler
.IR \*pas "(1) and "
the link editor
.IR \*pld (1).
Both programs will make 
.B a.out
executable if there were no errors in assembling or linking and no unresolved
external references.
.PP
A 3B-20 object file consists of a file header, \s-1UNIX\s+1 header, a table of 
section headers, relocation information, line numbers, and a symbol table.
The order is given below.
.sp 1v
.nf
.RS 10
File header.
.if '\*p'' \s-1UNIX\s+1 header.
Section 1 header.
\&...
Section n header.
Section 1 data.
\&...
Section n data.
Section 1 relocation.
\&...
Section n relocation.
Section 1 line numbers.
\&...
Section n line numbers.
Symbol table.
.sp 1v
.fi
.RE
.ta
.lg
.if t .RE
The last two sections may be missing
if the program was linked
with the
.B \-s
option of
.IR ld (1)
or if the symbol table and relocation bits were removed by
.IR strip (1).
Also note that if there were no unresolved external references after linking,
the relocation information will be removed.
.PP
The sizes of each segment (contained in the header, discussed below)
are in bytes and are even.
.PP
When an
.B a.out
file is loaded into memory for execution, three logical segments are
set up: the text segment, the data segment
(initialized data followed by uninitialized, the latter actually being
initialized to all 0's),
and a stack.
The text segment begins at location 0
in the core image; the header is not loaded.
If the magic number (the first field in the header) is 407 (octal), it
indicates that the text segment is not to be write-protected or shared,
so the data segment will be contiguous with the text segment.
If the magic number is 410 (octal),
the data segment begins at the next segment boundary
( a segment is 128 K )
following the text segment,
and the text segment is not writable by the program;
if other processes are executing the same
.B a.out
file, they will share a single text segment.
If the magic number is 411 (octal) (PDP-11 only),
the text segment
is again pure
(write-protected and shared)
and, moreover,
the instruction and data spaces
are separated;
the text and data segment both
begin at location 0.
.PP
The stack begins at the end of the text and data sections
on the 3B-20
and grows towards higher addresses.
The stack is automatically extended as required.
The data segment is extended only as requested by
the
.IR brk (2)
system call.
.PP
The start of the text segment in the
.B a.out
file is
.IR hsize;
the start of
the data segment
is
.IR hsize+St
(the size of the text),
where
.IR hsize
is 270 (octal) on the 3B-20.
.PP
The value of a word in the text or data portions that is not
a reference to an undefined external symbol
is exactly the value that will appear in memory
when the file is executed.
If a word in the text 
involves a reference to an undefined external symbol,
the storage class of the symbol table entry for
that word will be marked as an ``external symbol'', 
and the section number will be set to 0.
When the file is processed by the
link editor and the external symbol becomes
defined, the value of the symbol will
be added to the word in the file.
.ne 12v
.SS File Header - 3B-20
The format of the 
.B filehdr.5
header for the 3B-20
is as follows:
.br
.ne 11v
.PP
.if t .RS
.ta \w'struct\ \ 'u +\w'unsigned'u +\w'\ short\ \ 'u +\w'f_symptr;\ \ 'u
.nf
.lg 0
struct filehdr
{
	unsigned short	f_magic;	/\(** magic number \(**/
	unsigned short	f_nscns;	/\(** number of sections \(**/
	long		f_timdat;	/\(** time and date stamp \(**/
	long		f_symptr;	/\(** file ptr to symtab \(**/
	long		f_nsyms;	/\(** # symtab entries \(**/
	unsigned short	f_opthdr;	/\(** sizeof(opt hdr) \(**/
	unsigned short	f_flags;	/\(** flags \(**/
};
.fi
.RE
.SS UNIX Header - 3B-20
The format of the 
.B \s-1UNIX\s+1 
header on the 3B-20 is as follows:
.br
.ne 13v
.PP
.if t .RS
.nf
.lg 0
typedef struct aouthdr {
	short	magic;		/\(** see magic.h \(**/
	short 	flags;		/\(** see below \(**/
	long	tsize;		/\(** text size in bytes, padded to FW bdry \(**/
	long	dsize;		/\(** initialized data " " \(**/
	long	bsize;		/\(** uninitialized data " " \(**/
	long	nsize;		/\(** nsyms table size in bytes \(**/
	long	ssize;		/\(** ssyms table size in bytes \(**/
	long	entry;		/\(** entry pt. \(**/
	long	text_start;	/\(** base of text used for this file \(**/
	long	data_start;	/\(** base of data used for this file \(**/
} \s-1AOUTHDR\s+1;
.fi
.lg
.RE
.PP
.SS Section Header - 3B-20
The format of the 
.B section
header on the 3B-20
is as follows:
.br
.ne 14v
.PP
.if t .RS
.ta \w'struct\ \ 'u +\w'unsign'u +\w'ed\ short\ \ 'u +\w's_lnnoptr;\ \ 'u
.nf
.lg 0
struct scnhdr
{
	char		s_name[8];
	long		s_paddr;	/\(** physical address \(**/
	long		s_vaddr;	/\(** virtual address \(**/
	long		s_size;	/\(** section size \(**/
	long		s_scnptr;	/\(** file ptr to raw data \(**/
	long		s_relptr;	/\(** file ptr to relocation \(**/
	long		s_lnnoptr;	/\(** file ptr to line numbers \(**/
	unsigned short	s_nreloc;	/\(** # reloc entries \(**/
	unsigned short	s_nlnno;	/\(** # line number entries \(**/
	long		s_flags;	/\(** flags \(**/
};
.fi
.lg
.RE
.br
.ne 12v
.SS Relocation - 3B-20
Object files have one relocation entry for each relocatable
reference in the text or data.
If relocation information is present, it will be in the
following format:
.PP
.if t .RS
.ta \w'#define\ \ 'u +\w'R_DIR32S\ \ 'u +\w'r_symndx;\ \ 'u
.nf
.lg 0
struct reloc
{
	long	r_vaddr;	/\(** (virtual) address of reference \(**/
	long	r_symndx;	/\(** index into symbol table \(**/
	short	r_type;	/\(** relocation type \(**/
};
.if '\*p'b16' \{\
#define	R_ABS	0
#define	R_DIR16	01
#define	R_REL16	02
#define	R_IND16	03\}
.fi 
.DT
.lg
.if t .RE
.PP
The start of the relocation information is \fIrelptr\fP from the Section
Header.
If there is no relocation information, \fIrelptr\fP is 0.
.SS Symbol Table - 3B-20
The format of the 
.B symbol table
header for the 3B-20
is as follows:
.br
.ne 13v
.PP
.if t .RS
.ta \w'#define\ \ 'u +\w'SYMNMLEN\ \ 'u +\w'n_numaux;\ \ 'u
.nf
.lg 0
#define  \s-1SYMNMLEN\s+1	8
#define  \s-1FILNMLEN\s+1	14

struct syment
{
	char		n_name[\s-1SYMNMLEN\s+1];
	long		n_value;	/\(** value of symbol \(**/
	short		n_scnum;	/\(** section number \(**/
	unsigned short	n_type;	/\(** type and derived type \(**/
	char		n_sclass;	/\(** storage class \(**/
	char		n_numaux;	/\(** number of aux entries \(**/
};
.fi
.DT
.lg
.if t .RE
.PP
Some symbols require more information than a single
entry; they are followed by
.I "auxiliary entries"
that are the same size as a symbol entry.
The format follows:
.br
.ne 38v
.PP
.if t .RS
.ta \w'struct\ 'u +\w'struct\ 'u +\w'unsigne'u +\w'd\ short\ \ 'u +\w'unsigne'u +\w'd\ short\ \ 'u
.nf
.lg 0
union auxent {
	struct {
		long	x_tagndx;
		union {
			struct {
				unsigned short	x_lnno;
				unsigned short	x_size;
			} x_lnsz;
			long	x_fsize;
		} x_misc;
		union {
			struct {
				long	x_lnnoptr;
				long	x_endndx;
			}  x_fcn;
			struct {
				unsigned short	x_dimen[\s-1DIMNUM\s+1];
			} x_ary;
		} x_fcnary;
		unsigned short  x_tvndx;
	} x_sym;

	struct {
		char	x_fname[\s-1FILNMLEN\s+1];
	} x_file;

	struct {
		long	    x_scnlen;	  
		unsigned short  x_nreloc;  
		unsigned short  x_nlinno;  
	} x_scn;

	struct {
		unsigned short	x_tvlen;
		unsigned short	x_tvran[2];
	} x_tv;
};
.fi
.DT
.lg
.if t .RE
.PP
Indexes of symbol table entries begin at
.IR zero .
The start of the symbol table is \fIsymptr\fP from the file header.
If the symbol table is stripped, \fIsymptr\fP is 0.
.SH SEE ALSO
as(1),
cc(1),
ld(1),
ldfcn(5).
'\" \%W\%
