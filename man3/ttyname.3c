.TH TTYNAME 3C 
.SH NAME
ttyname, isatty \- find name of a terminal
.SH SYNOPSIS
.B char \(**ttyname (fildes)
.PP
.B int isatty (fildes)
.SH DESCRIPTION
.I Ttyname\^
returns a pointer to the null-terminated path name
of the terminal device associated with file descriptor
.IR fildes .
.PP
.I Isatty\^
returns 1 if
.I fildes\^
is associated with a terminal device, 0 otherwise.
.SH FILES
/dev/\(**
.SH DIAGNOSTICS
.I Ttyname\^
returns a null pointer (0) if
.I fildes\^
does not describe a terminal device in directory
.BR /dev .
.SH BUGS
The return value points to static data
whose content is overwritten by each call.
