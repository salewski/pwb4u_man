.TH CTIME 3C
.SH NAME
ctime, localtime, gmtime, asctime, tzset \- convert date and time to \s-1ASCII\s0
.SH SYNOPSIS
.B char \(**ctime (clock)
.br
.B long \(**clock;
.PP
.B #include <time.h>
.PP
.B struct tm \(**localtime (clock)
.br
.B long \(**clock;
.PP
.B struct tm \(**gmtime (clock)
.br
.B long \(**clock;
.PP
.B char \(**asctime (tm)
.br
.B struct tm \(**tm;
.PP
.B tzset ( )
.SH DESCRIPTION
.I Ctime\^
converts a time pointed to by
.I clock\^
such as returned by
.IR time (2)
into \s-1ASCII\s0
and returns a pointer to a
26-character string
in the following form.
All the fields have constant width.
.PP
.RS
Sun Sep 16 01:03:52 1973\\n\\0
.RE
.PP
.I Localtime\^
and
.I gmtime\^
return pointers to structures containing
the broken-down time.
.I Localtime\^
corrects for the time zone and possible daylight savings time;
.I gmtime\^
converts directly to \s-1GMT\s0, which is the
time the \s-1UNIX\s0 system uses.
.I Asctime\^
converts a broken-down time to \s-1ASCII\s0 and returns a pointer
to a 26-character string.
.PP
The structure declaration from the include file is:
.RS
.PP
.nf
struct tm {
        int tm_sec;
        int tm_min;
        int tm_hour;
        int tm_mday;
        int tm_mon;
        int tm_year;
        int tm_wday;
        int tm_yday;
        int tm_isdst;
};
extern struct tm \(**gmtime(), \(**localtime();
extern char \(**ctime(), \*(asctime;
extern void tzset();
extern long timezone;
extern int daylight;
extern char \(**tzname[];
.RE
.PP
These quantities give the time on a 24-hour clock,
day of month (1-31), month of year (0-11), day of week
(Sunday = 0), year \- 1900, day of year (0-365),
and a flag that is non-zero if daylight saving time is in effect.
.PP
The external
.B long
variable
.I timezone\^
contains the difference, in seconds, between \s-1GMT\s0 and local
standard time (in \s-1EST\s0,
.I timezone\^
is 5\(**60\(**60);
the external variable
.I daylight\^
is non-zero if and only if the standard
.SM U.S.A.
Daylight Savings Time conversion should be applied.
The program knows about the peculiarities
of this conversion in 1974 and 1975;
if necessary,
a table for these years can be extended.
.PP
If an environment variable named
.SM
.B TZ
is present,
.I asctime\^
uses the contents of the variable to override the default time
zone.
The value of
.SM
.B TZ
must be a three-letter time zone name, followed by a number
representing the difference between local time and Greenwich
time in hours, followed by an optional three-letter name for
a daylight time zone.
For example, the setting for New Jersey would be
.SM
.BR EST5EDT .
The effects of setting
.SM
.B TZ
are thus to change the values of the external variables
.I timezone\^
and
.IR daylight ;
in addition, the time zone names contained in the external variable
.PP
.B
 	char \(**tzname[2] = {"\s-1EST\s0", "\s-1EDT\s0"};
.PP
are set from the environment variable.
The function
.I tzset\^
sets the external variables from
.SM
.BR TZ ;
it is called by
.I asctime\^
and may also be called explicitly by the user.
.PP
.SH "SEE ALSO"
time(2), getenv(3C), environ(5).
.SH BUGS
The return values point to static data
whose content is overwritten by each call.
