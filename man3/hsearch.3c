.TH HSEARCH 3C 
.SH NAME
hsearch, hcreate, hdestroy \- manage hash search tables 
.SH SYNOPSIS
.br
typedef struct entry {
        char \(**key;
        char \(**data;
} \s-1ENTRY\s+1;
.br
typedef enum {
        \s-1FIND\s+1,
        \s-1ENTER\s+1
} \s-1ACTION\s+1;
.br
.B \s-1ENTRY\s+1 \(**hsearch (item, action)
        \s-1ENTRY\s+1 item;
.br
        \s-1ACTION\s+1 action;
.br
.B int hcreate (nel)
.br
        int nel;
.br
.B void hdestroy ()
.SH DESCRIPTION
.I Hsearch\^
is a hash table search routine generalized from Knuth (6.4) Algorithm D.
It returns a pointer into a hash table indicating the location at which
an entry can be found.
The first argument
is a structure
containing two pointers,
one to the comparison key
and one to any other data to be associated with that key.
The second argument is a flag
indicating the disposition of the entry if it cannot be found in the table.
.SM
.B ENTER
indicates that the item should be inserted in the table at an
appropriate point.
.SM
.B FIND
indicates that no entry should be made.
Unsuccessful resolution is 
indicated by the return of a 
.SM NULL
pointer.
.P
The user must allocate sufficient space
for the table by calling
.I hcreate\^
before
.I hsearch\^
is used.
.I Hcreate\^
takes one argument,
an estimate of the maximum number of entries that
the table will contain.
This number may be adjusted upward by the
algorithm in order to obtain certain mathematically favorable
circumstances.
.P
.I Hsearch\^
uses
.I open addressing\^
with a
.I multiplicative\^
hash function.
Its source code, however, has many other options
available which the user may select by compiling the
.I hsearch\^
source with the following symbols defined to the preprocessor:
.RS
.TP \w'\s-1CHAINED\s+1\ \ 'u
.B \s-1DIV\s+1
use the
.I remainder modulo table size\^
as the hash function insteadof the multiplicative algorithm.
.TP
.B \s-1USCR\s+1
use a User Supplied Comparison Routine for ascertaining
table membership.
The routine should be named
.I hcompar\^
and should behave in a mannner similar to
.IR strcmp .
.TP
.B \s-1CHAINED\s+1
use a linked list to resolve collisions.
If this option is selected,
the following other options become available.
.RS
.TP \w'\s-1SORTDOWN\s+1\ \ 'u
.B \s-1START\s+1
place new entries at the beginning of the linked list (default is at
the end).
.TP
.B \s-1SORTUP\s+1
keep the linked list sorted by key in ascending order.
.TP
.B \s-1SORTDOWN\s+1
keep the linked list sorted by key in descending order.
.RE
.RE
.P
Additionally, there are preprocessor flags for obtaining debugging
printout
.RB ( \-\s-1DDEBUG\s+1 )
and for including a test driver in the calling routine
.RB ( \-\s-1DDRIVER\s+1 ).
The source code should be consulted for further details.
.SH "SEE ALSO"
bsearch(3C),
lsearch(3C),
qsort(3C),
string(3C),
tsearch(3C).
.SH DIAGNOSTICS
.I Hsearch\^
returns a
.SM NULL
pointer if either the action is
.SM
.B FIND
and the item could not be found or the action is
.SM
.B ENTER
and the table is full.
.P
.I Hcreate\^
returns a
.SM NULL
pointer if it cannot allocate sufficient space for the
table.
