.TH UUSTAT 1C
.SH NAME
uustat \- uucp status inquiry and job control
.SH SYNOPSIS
.B uustat
[ options ] 
.SH DESCRIPTION
.I Uustat\^
will display the status of, or cancel, previously specified
.I uucp\^
commands, or provide general status on 
.I uucp\^
connections to other systems.
The following options are recognized:
.PP
.PD 0
.TP 10
.BI \-m mch\^
Report the status of accessibility of machine
.IR mch .
If
.I mch\^
is specified as
.BR all ,
then the status of all machines known to the
local
.I uucp\^
are provided.
.TP 10
.BI \-k jobn\^
Kill the
.I uucp\^
request whose job number is
.IR jobn .
The killed
.I uucp\^
request must belong to the person
issuing the
.I uustat\^
command
unless he is the super-user.
.TP 10
.BI \-c hour\^
Remove the status entries which are older than
.I hour\^
hours.
This administrative option can only be initiated by the user
.B uucp
or the super-user.
.TP 10
.BI \-u user\^
Report the status of all
.I uucp\^
requests issued by
.IR user .
.TP 10
.BI \-s sys\^
Report the status of all
.I uucp\^
requests which communicate with remote system
.IR sys .
.TP 10
.BI \-o hour\^
Report the status of all
.I uucp\^
requests which are older than
.I hour\^
hours.
.TP 10
.BI \-y hour\^
Report the status of all
.I uucp\^
requests which are younger than
.I hour\^
hours.
.TP 10
.BI \-j all\^
Report the status of 
all the
.I uucp\^
requests.
.TP
.B \-v
Report the 
.I uucp\^
status verbosely.
If this option is not specified, a status code is printed
with each
.I uucp\^
request.
.PD 0
.PP
When no options are given,
.I uustat\^
outputs the status of all
.I uucp\^
requests issued by the current user.
Note that only one of the options
.BR \-j ,
.BR \-m ,
.BR \-k ,
.BR \-c ,
or the rest of other options
may be specified.
.PD
.PP
For example, the command
.RS
.PP
uustat \-uhdc \-smhtsa \-y72 \-v
.RE
.PP
will print the verbose status of all
.I uucp\^
requests that were issued by user 
.I hdc\^
to communicate with system
.I mhtsa\^
within the last 72 hours.
The meanings of the job request status are:
.RS
.PP
job-number user remote-system command-time status-time status
.RE
.PP
where the
.I status\^
may be either an octal number or a verbose
description.
The octal code corresponds to the following description:
.PP
.PD 0
.TP 10
.SM OCTAL
.SM STATUS
.TP 10
00001
the copy failed, but the reason cannot be determined
.TP 10
00002
permission to access local file is denied
.TP 10
00004
permission to access remote file is denied
.TP 10
00010
bad
.I uucp\^
command is generated
.TP 10
00020
remote system cannot create temporary file
.TP 10
00040
cannot copy to remote directory
.TP 10
00100
cannot copy to local directory
.TP 10
00200
local system cannot create temporary file
.TP 10
00400
cannot execute
.I uucp\^
.TP 10
01000
copy succeeded
.TP 10
02000
copy finished, job deleted
.TP 10
04000
job is queued
.PD 0
 
.PD
.PP
The meanings of the machine accessibility status are:
.RS
.PP
system-name time status
.RE
.PP
where \fItime\fR is the latest status time and
.I status\^
is a self-explanatory description of the machine status.
.SH FILES
.PD 0
.TP 1.5i
/usr/spool/uucp
spool directory
.TP
/usr/lib/uucp/L_stat
system status file
.TP
/usr/lib/uucp/R_stat
request status file
.PD
.SH SEE ALSO
uucp(1C).
